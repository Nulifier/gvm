[CmdletBinding(PositionalBinding=$false, DefaultParameterSetName="stable")]
param (
	[string][ValidatePattern("^\d+(?:\.\d+)+$")]$version,
	[switch][Parameter(ParameterSetName="stable")]$stable,
	[int][Parameter(ParameterSetName="alpha")][ValidateRange(1,100)]$alpha,
	[int][Parameter(ParameterSetName="beta")][ValidateRange(1,100)]$beta,
	[int][Parameter(ParameterSetName="rc")][ValidateRange(1,100)]$rc,
	[string][ValidateSet("linux_headless", "linux_server", "osx32", "osx.fat", "osx.64", "osx.universal", "win32", "win64", "x11.32", "x11.64")]$platform,
	[switch]$mono,
	[switch]$nomono,
	[switch]$save,
	[switch]$download,
	[switch]$clean,
	[switch]$norun,
	[switch]$noeditor,
	[string[]][Parameter(Position=0, ValueFromRemainingArguments = $true)]$godotArgs
)

function Get-Version {
	if (Test-Path -Path .godotversion) {
		$stored = (Get-Content ".godotversion" | Select-String -Pattern "^Godot_v(?<version>[0-9.]+)[-_](?<subversion>[a-z0-9]+)(?:_(?<mono>mono))?_(?<platform>.+)").Matches.Groups
		$selectedVersion = $stored | Select-Object -Skip 1 | Foreach-Object -Begin {$h = @{} } -Process {
			$h.Add($_.Name, $_.Value.Trim())
		} -End {
			[PSCustomObject]$h
		}
	}
	else {
		# Defaults
		$selectedVersion = @{
			# Latest stable version as of writing
			version="3.5"
			subversion="stable"
			# No mono
			mono=$false
			# Windows with a bit depth matching OS
			platform=if ([Environment]::Is64BitOperatingSystem) {"win64"} else {"win32"}
		}
	}

	# Overrides
	if ($version) {$selectedVersion.version = $version}
	if ($stable) {$selectedVersion.subversion = "stable"}
	if ($alpha) {$selectedVersion.subversion = "alpha$alpha"}
	if ($beta) {$selectedVersion.subversion = "beta$beta"}
	if ($rc) {$selectedVersion.subversion = "rc$rc"}
	if ($mono) {$selectedVersion.mono = $true}
	if ($nomono) {$selectedVersion.mono = $false}
	if ($platform) {$selectedVersion.platform = $platform}
	
	$selectedVersion
}

function Get-FullVersionName($selectedVersion, [switch]$executable, [switch]$zip, [switch]$url) {
	# Early versions of Godot used an underscore to separate the version fron the subversion, later ones used a dash
	$underscoreVersions = "1.1","2.0","2.0.1","2.0.2","2.0.3","2.0.4.1"
	$separator = if ($underscoreVersions.Contains($selectedVersion.version)) {"_"} else {"-"}

	$mono = if ($selectedVersion.mono) {"_mono"} else {""}

	$name = "Godot_v$($selectedVersion.version)$separator$($selectedVersion.subversion)${mono}_$($selectedVersion.platform)"

	$windows = ($selectedVersion.platform -eq "win32") -or ($selectedVersion.platform -eq "win64")
	if ($executable -and $windows) {
		$name += ".exe"
	}

	if ($zip -or $url) {
		# Only windows requires an extra extension but not when its the mono version
		if ($windows -and !$selectedVersion.mono) {
			$name += ".exe"
		}
		$name += ".zip"
	}

	if ($url) {
		$subversion = if ($selectedVersion.subversion -ne "stable") {"/$($selectedVersion.subversion)"} else {""}
		$mono = if ($selectedVersion.mono) {"/mono"} else {""}
		$name = "https://downloads.tuxfamily.org/godotengine/$($selectedVersion.version)$subversion$mono/$name"
	}

	$name
}

function Request-GodotVersion($selectedVersion) {
	$name = Get-FullVersionName $selectedVersion
	$exe = Get-FullVersionName $selectedVersion -executable
	$folder = "$($env:LOCALAPPDATA)\gvm\$name"

	# If we are forced to download, delete whatever is there already
	if ($download) {
		Remove-Item $folder -Force -Recurse -ErrorAction SilentlyContinue
	}

	# Test if we already have this version
	$versionExists = Test-Path -Path $folder
	if ((-not $versionExists) -or $download) {
		$url = Get-FullVersionName $selectedVersion -url

		Write-Host "Downloading $url"

		$tmp = New-TemporaryFile | Rename-Item -NewName {$_ -replace "tmp$", "zip"} -PassThru
		try {
			$ProgressPreference = "SilentlyContinue"
			Invoke-WebRequest -Uri $url -OutFile $tmp
			$ProgressPreference = "Continue"
			if ($selectedVersion.mono) {
				# Mono releases have a nested folder
				$tmpFolder = New-TemporaryFile |% { Remove-Item $_; New-Item -ItemType Directory -Path $_ }

				$tmp | Expand-Archive -DestinationPath $tmpFolder -Force
				New-Item -ItemType Directory -Path $folder
				Move-Item -Path "$tmpFolder/$name/*" -Destination $folder

				$tmpFolder | Remove-Item -Recurse -Force
			}
			else {
				$tmp | Expand-Archive -DestinationPath $folder -Force
			}
		}
		catch {
			Write-Host "Could not download the release, check this is a valid release"
			Write-Host "Error:"
			Write-Host $_
		}
		$tmp | Remove-Item
	}

	return "$folder\$exe"
}

$selectedVersion = Get-Version

if ($save) {
	Get-FullVersionName $selectedVersion | Out-File -FilePath .godotversion -Encoding ASCII -NoNewline
}

if ($clean) {
	$name = Get-FullVersionName $selectedVersion
	Get-ChildItem -Path "$($env:LOCALAPPDATA)\gvm" -Exclude $name -Directory | Remove-Item -Force -Recurse
}

$exePath = Request-GodotVersion $selectedVersion

if (!$norun) {
	try {
		if ($noeditor) {
			& "${exePath}" @godotArgs | Write-Host	
		}
		else {
			& "${exePath}" --editor @godotArgs | Write-Host
		}
	}
	catch {
		Write-Host "Error running Godot, could be the wrong platform"
		Write-Host "Error:"
		Write-Host $_
	}
}
