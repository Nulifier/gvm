#!/bin/bash

while [ 1 ]; do
	if [ "$1" = "-help" -o "$1" = "-h" ]; then
		echo "Godot Version Manager v1.0"
		echo ""
		echo "Usage: gvm [-opts] [--] [godot args]"
		echo "  Default action is to get a saved or recent version of Godot and run the"
		echo "  editor, passing any extra arguments."
		echo ""
		echo "Options:"
		echo " -v, -version [S]  the version of Godot to launch"
		echo "     -stable       stable version, as opposed to alpha, beta or rc"
		echo " -a, -alpha [#]    alpha version, [#] is the alpha number"
		echo " -b, -beta [#]     beta version, [#] is the beta number"
		echo " -r, -rc [#]       release candidate, [#] is the rc number"
		echo " -p, -platform [S] which platform to use, can be: linux_headless, linux_server,"
		echo "                       osx32, osx.fat, osx.64, osx.universal, win32, win64,"
		echo "                       x11.32, x11.64"
		echo " -m, -mono         mono version"
		echo "     -nomono       no mono version"
		echo "     -save         save the selected version to a .godotversion file"
		echo " -d, -download     download the release even if we already have it"
		echo " -c, -clean        remove all other versions that are not the selected one"
		echo "     -norun        do not run Godot after completion"
		echo "     -noeditor     change the default behaviour back to Godot's (run the game)"
		exit 0
	elif [ "$1" = "-version" -o "$1" = "-v" ]; then
		opt_version=$2
		shift 2
		if ! [[ $opt_version =~ ^[[:digit:]]+(\.[[:digit:]]+)+$ ]]; then
			echo "Invalid version '$opt_version' provided"
			exit 1
		fi
	elif [ "$1" = "-stable" ]; then
		opt_stable=1
		shift 1
	elif [ "$1" = "-alpha" -o "$1" = "-a" ]; then
		if [[ $2 -le 0 ]]; then
			echo "Invalid subversion '$2', must be a positive integer"
			exit 1
		fi
		opt_alpha=$2
		shift 2
	elif [ "$1" = "-beta" -o "$1" = "-b" ]; then
		if [[ $2 -le 0 ]]; then
			echo "Invalid subversion '$2', must be a positive integer"
			exit 1
		fi
		opt_beta=$2
		shift 2
	elif [ "$1" = "-rc" -o "$1" = "-r" ]; then
		if [[ $2 -le 0 ]]; then
			echo "Invalid subversion '$2', must be a positive integer"
			exit 1
		fi
		opt_rc=$2
		shift 2
	elif [ "$1" = "-platform" -o "$1" = "-p" ]; then
		opt_platform=$2
		shift 2
		if ! [[ $opt_platform =~ ^(linux_headless|linux_server|osx32|osx\.fat|osx\.64|osx\.universal|win32|win64|x11\.32|x11\.64)$ ]]; then
			echo "Invalid platform provided, must be one of linux_headless, linux_server, osx32, osx.fat, osx.64, osx.universal, win32, win64, x11.32, or x11.64"
			exit 1
		fi
	elif [ "$1" = "-mono" -o "$1" = "-m" ]; then
		opt_mono=1
		shift 1
	elif [ "$1" = "-nomono" ]; then
		opt_nomono=1
		shift 1
	elif [ "$1" = "-save" ]; then
		opt_save=1
		shift 1
	elif [ "$1" = "-download" -o "$1" = "-d" ]; then
		opt_download=1
		shift 1
	elif [ "$1" = "-clean" -o "$1" = "-d" ]; then
		opt_clean=1
		shift 1
	elif [ "$1" = "-norun" ]; then
		opt_norun=1
		shift 1
	elif [ "$1" = "-noeditor" ]; then
		opt_noeditor=1
		shift 1
	elif [ "$1" = "--" ]; then
		shift 1
		break
	else
		break
	fi
done

get_version () {
	if [ -f ".godotversion" ]; then
		stored=$(cat .godotversion)
		local regex="Godot_v([[:digit:]\.]+)[-_]([[:digit:][:alpha:]]+)_(.+)"
		local mono_regex="Godot_v([[:digit:]\.]+)[-_]([[:digit:][:alpha:]]+)_mono_(.+)"
		
		# Test the mono version first
		if [[ $stored =~ $mono_regex ]]; then
			mono=1
		elif [[ $stored =~ $regex ]]; then
			mono=0
		else
			echo "Invalid version number stored in .godotversion"
			exit 1
		fi
		
		version=${BASH_REMATCH[1]}
		subversion=${BASH_REMATCH[2]}
		platform=${BASH_REMATCH[3]}
	else
		# Assign some defaults
		mono=0
		version="3.5"
		subversion="stable"
		if [[ $(uname -s) == "Darwin"* ]]; then
			# Doesn't handle old versions of mac that use bit depth, if there
			# is interest I can add it
			platform="osx.universal"
		elif [[ $(uname -m) = "x86_64" ]]; then
			platform="x11.64"
		else
			platform="x11.32"
		fi
	fi

	# Overrides
	if [ -n "$opt_version" ]; then version=$opt_version; fi
	if [ -n "$opt_stable" ]; then subversion="stable"; fi
	if [ -n "$opt_alpha" ]; then subversion="alpha$opt_alpha"; fi
	if [ -n "$opt_beta" ]; then subversion="beta$opt_beta"; fi
	if [ -n "$opt_rc" ]; then subversion="rc$opt_rc"; fi
	if [ -n "$opt_mono" ]; then mono=1; fi
	if [ -n "$opt_nomono" ]; then mono=0; fi
	if [ -n "$opt_platform" ]; then platform=$opt_platform; fi
}

get_version_name () {
	# Early versions of Godot used an underscore to separate the version fron the subversion, later ones used a dash
	[[ $version =~ ^(1\.|2\.0) ]] && separator="_" || separator="-"
	[[ $mono -eq 1 ]] && mono_part="_mono" || mono_part=""
	echo "Godot_v$version$separator$subversion${mono_part}_$platform"
}

get_version_exe () {
	local name=$(get_version_name)
	if [ $platform = "win32" -o $platform = "win64" ]; then name=$name.exe; fi
	echo $name
}

get_version_zip () {
	local name=$(get_version_name)
	# Windows requires the extension but only when no mono
	if [ $platform = "win32" -o $platform = "win64" ] && ! [ $mono -eq 1 ]; then name=$name.exe; fi
	# X11 changes from x11.32 to x11_32 in mono builds
	if [ $mono -eq 1 ]; then
		name=${name/%x11.32/x11_32}
		name=${name/%x11.64/x11_64}
	fi

	echo "$name.zip"
}

get_version_url () {
	local sub=""
	local mon=""
	if [[ $subversion -ne "stable" ]]; then sub="/$subversion"; fi
	if [[ $mono -eq 1 ]]; then mon="/mono"; fi
	echo "https://downloads.tuxfamily.org/godotengine/$version$sub$mon/$(get_version_zip)"
}

request_godot_version () {
	local name=$(get_version_name)
	local folder="$HOME/.gvm/$name"
	exe_path="$folder/$(get_version_exe)"

	# If we are forced to download, delete whatever is there already
	if [[ $opt_download ]]; then
		rm -rf $folder
	fi

	# Test if we already have this version
	if [ ! -d "$folder" ]; then
		local url=$(get_version_url)

		echo "Downloading $url"
		local tmp=$(mktemp)
		if ! curl -# --fail -o $tmp "$url"; then
			rm $tmp
			echo "Could not download the release, check this is a valid release"
			exit 1
		fi

		# Make the destination folder
		mkdir -p $folder

		if [[ $mono -eq 1 ]]; then
			# Mono releases have a nested folder
			local tmpdir=$(mktemp -d)

			unzip -q $tmp -d $tmpdir
			cp -r $tmpdir/*/* $folder

			ls -al $tmpdir

			rm -r $tmpdir
		else
			unzip -q $tmp -d $folder
		fi

		rm $tmp

		# Make it executable
		chmod +x $exe_path
	fi
}

get_version

if [[ $opt_save -eq 1 ]]; then
	printf $(get_version_name) > .godotversion
fi

if [[ $opt_clean -eq 1 ]]; then
	find $HOME/.gvm/ -mindepth 1 -maxdepth 1 -type d -not -name "$(get_version_name)" -exec rm -rf {} \;
fi

request_godot_version

if [ -z "$opt_norun" ]; then
	if [ $opt_noeditor -eq 1 ]; then
		$exe_path $@
	else
		$exe_path --editor $@
	fi
fi
