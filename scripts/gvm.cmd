@ECHO OFF

:: Unblock the PowerShell file so we can execute it with RemoteSigned policy
echo. > %~dp0\gvm.ps1:Zone.Identifier

:: This is just a simple file to execute a powershell command, bypassing the execution policy
PowerShell.exe -ExecutionPolicy Bypass -File %~dp0\gvm.ps1 %*
