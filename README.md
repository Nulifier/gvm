# Godot Version Manager (gvm)

`gvm` allows you to fix the version of [Godot](https://godotengine.org/) that
you are using in a project with a small `.godotversion` file rather than
including the executable. It handles downloading the executable and can have
multiple versions available simultaneously.

## Usage

Typical usage will just be running the gvm script as it will open Godot and
forward any arguments. If you run it without any arguments it will first look
for a saved `.godotversion` file and if it doesn't find one, it will make some
reasonable assumptions, download Godot and start it.

If you want to customize the version of godot that is used, you can use the
command flags below. Once you've found your ideal version, you can use the
`-save` flag to write a `.godotversion` file and check that into source control.
Subsequent uses of `gvm` will automatically use that version.

## Command Flags

On the PowerShell version of `gvm`, these options will be passed to `gvm` and
all other options will be forwarded to Godot directly. The only overlap is the
`-version` flag.
