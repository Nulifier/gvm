#!/bin/bash

# This script builds a standalone version of the batch file so you don't need
# to download two files

version="1.0"

printf "@ECHO OFF\n" > gvm.cmd
printf "SETLOCAL\n\n" >> gvm.cmd

printf "SET SCRIPT=%%LOCALAPPDATA%%\\gvm\\gvm-$version.ps1\n" >> gvm.cmd
printf "SET TMPENC=%%SCRIPT%%.b64\n\n" >> gvm.cmd

printf 'if not exist "%%SCRIPT%%" (\n' >> gvm.cmd

printf 'mkdir %%LOCALAPPDATA%%\\gvm 2>nul\n\n' >> gvm.cmd

printf "REM I know this looks pretty sketchy seeing a big pile of base64 sitting in a\n" >> gvm.cmd
printf "REM file, if you want to see whats in here, its the base 64 version of\n" >> gvm.cmd
printf "REM https://gitlab.com/Nulifier/gvm/-/blob/master/scripts/gvm.ps1\n\n" >> gvm.cmd

printf "ECHO.>%%TMPENC%%\n" >> gvm.cmd
# We want a max of 120 characters in a line due to a limitation of batch files
base64 -w 104 scripts/gvm.ps1 | xargs -i -n1 printf 'ECHO {}>>%%TMPENC%%\n' >> gvm.cmd

printf '\ncertutil -decode %%TMPENC%% %%SCRIPT%% 1>nul\n' >> gvm.cmd
printf 'DEL %%TMPENC%%\n\n' >> gvm.cmd

# Something to unblock the file if its flagged as downloaded from the internet
printf 'REM Unblock the PowerShell file so we can execute it with RemoteSigned policy\n' >> gvm.cmd
printf 'echo. > %%SCRIPT%%:Zone.Identifier\n' >> gvm.cmd

printf ')\n\n' >> gvm.cmd	# End if

# Now the part that actually runs the script
printf 'REM Execute the PowerShell command, bypassing the security policy\n' >> gvm.cmd
printf 'PowerShell.exe -ExecutionPolicy Bypass -File %%SCRIPT%% %%*\n\n' >> gvm.cmd
printf 'ENDLOCAL\n' >> gvm.cmd
